FROM alpine:3.6

RUN apk add --no-cache \
    curl \
    gettext \
    nginx \
    php7 \
    php7-json \
    php7-phar \
    php7-iconv \
    php7-mbstring \
    php7-openssl \
    php7-zlib \
    php7-gd \
    php7-curl \
    php7-pdo \
    php7-pdo_mysql \
    php7-mysqlnd \
    php7-mcrypt \
    php7-common \
    php7-dom \
    php7-iconv \
    php7-ctype \
    php7-xml \
    php7-opcache \
    php7-tokenizer \
    php7-xmlwriter \
    php7-session \
    php7-simplexml \
    php7-intl \
    php7-fileinfo \
    php7-tidy \
    php7-fpm

# install composer
ENV COMPOSER_VERSION="1.5.2" \
    COMPOSER_ALLOW_SUPERUSER="1" \
    COMPOSER_HOME="/tmp"

RUN curl -fsSL https://raw.githubusercontent.com/composer/getcomposer.org/da290238de6d63faace0343efbdd5aa9354332c5/web/installer \
    | php -- \
        --install-dir=/usr/local/bin \
        --filename=composer \
        --version="${COMPOSER_VERSION}" \
    && composer --version --no-interaction

RUN mkdir -p \
        /run/nginx \
        /var/log/nginx \
        /var/log/php7 \
    && rm /etc/nginx/conf.d/*

RUN echo '' >> /etc/php7/php.ini \
    &&  echo '; Following lines are injected during docker build' >> /etc/php7/php.ini \
    &&  echo 'error_log = ${LOGS_DIR}/php-error.log' >> /etc/php7/php.ini \
    &&  echo 'date.timezone = ${PHP_DATE_TIMEZONE}' >> /etc/php7/php.ini

# copy settings
COPY ./config/nginx.conf.template /etc/nginx/nginx.conf.template
COPY ./config/site.conf /etc/nginx/conf.d/site.conf
COPY ./config/php-fpm-www.conf /etc/php7/php-fpm.d/www.conf
COPY ./config/docker-entrypoint.sh /docker-entrypoint.sh
COPY ./config/start.sh /start.sh

# Runtime configurable variables
ENV PHP_DATE_TIMEZONE="Australia/Melbourne" \
    LOGS_DIR="/site/logs"

WORKDIR /site
EXPOSE 80

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["/start.sh"]