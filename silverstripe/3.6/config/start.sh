#!/bin/sh

set -e

tail -f "$LOGS_DIR"/*.log &

php-fpm7 \
    --daemonize \
    --allow-to-run-as-root

nginx -g "daemon off;"