#!/bin/sh

set -e

# Setup logs
mkdir -p "$LOGS_DIR"
touch "${LOGS_DIR}/nginx-access.log"
touch "${LOGS_DIR}/nginx-error.log"
touch "${LOGS_DIR}/php-error.log"
touch "${LOGS_DIR}/silverstripe.log"

# Generate nginx.conf
envsubst '${LOGS_DIR}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

"$@"