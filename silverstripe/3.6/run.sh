#!/bin/sh

set -e

cd "$(dirname $0)"

CMD=${@-sh}

docker run --rm -it \
    --name ss \
    ehyland/dockerfiles/silverstripe:3.6 \
    $CMD