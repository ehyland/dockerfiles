#!/bin/sh

set -e

cd "$(dirname $0)"

REGISTRY="registry.gitlab.com/"
IMAGE_NAME="ehyland/dockerfiles/silverstripe"

SILVERSTRIPE_TARGET="4.1"

IMAGE_TAGGED="${IMAGE_NAME}:${SILVERSTRIPE_TARGET}"
IMAGE_DEBUG_TAGGED="${IMAGE_NAME}:${SILVERSTRIPE_TARGET}-debug"

# Build image
docker build -t "$IMAGE_TAGGED" .
docker build -t "$IMAGE_DEBUG_TAGGED"  -f ./Dockerfile-xdebug .

echo "--------------------------------------------------------------------------------"
echo "Built:"
echo "  - $IMAGE_TAGGED"
echo "  - $IMAGE_DEBUG_TAGGED"
echo "--------------------------------------------------------------------------------"

if [ "$1" == "--push" ]; then
    to_log=""
    for image in \
        "$IMAGE_TAGGED" \
        "$IMAGE_DEBUG_TAGGED"
    do
        image_with_registry="${REGISTRY}${image}"
        docker tag "$image" "$image_with_registry"
        docker push "$image_with_registry"
        to_log="${to_log}\n  - $image_with_registry"
    done
    echo "--------------------------------------------------------------------------------"
    echo -e "Published:${to_log}"
    echo "--------------------------------------------------------------------------------"
fi