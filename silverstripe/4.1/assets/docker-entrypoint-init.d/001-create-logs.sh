# Setup logs
mkdir -p "$LOGS_DIR"
touch "${LOGS_DIR}/nginx-access.log"
touch "${LOGS_DIR}/nginx-error.log"
touch "${LOGS_DIR}/php-error.log"
touch "${LOGS_DIR}/silverstripe.log"