#!/usr/bin/env bash

set -e

for f in /docker-command-init.d/*; do
    case "$f" in
        *.sh)     echo "$0: running $f"; . "$f" ;;
        *)        echo "$0: ignoring $f" ;;
    esac
done

nginx -g "daemon off;"