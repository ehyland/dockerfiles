#!/bin/sh

set -e

cd "$(dirname $0)"

CMD=${@-bash}

docker run --rm -it \
    --name ss \
    ehyland/dockerfiles/silverstripe:4.1 \
    $CMD