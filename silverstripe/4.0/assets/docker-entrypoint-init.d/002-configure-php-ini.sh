# modify php.ini
php_ini=(
    ""
    "; Following lines are injected by docker entrypoint:"
    "error_log = \"${LOGS_DIR}/php-error.log\""
    "memory_limit = \"${PHP_MEMORY_LIMIT}\""
    "date.timezone = \"${PHP_DATE_TIMEZONE}\""
    "display_errors = 0"
    "opcache.enable = 1"
    "upload_max_filesize = \"${PHP_UPLOAD_MAX_FILESIZE}\""
    "post_max_size = \"${PHP_POST_MAX_SIZE}\""
)

if [ -n "$PHP_SESSION_SAVE_HANDLER" ] && [ -n "$PHP_SESSION_SAVE_PATH" ]; then
    php_ini+=("session.save_handler = \"$PHP_SESSION_SAVE_HANDLER\"")
    php_ini+=("session.save_path = \"$PHP_SESSION_SAVE_PATH\"")
fi

for line in "${php_ini[@]}"; do
    echo "$line" >> /etc/php7/php.ini
done