#!/bin/sh

set -e

cd "$(dirname $0)"

REGISTRY="registry.gitlab.com/"
IMAGE="ehyland/dockerfiles/caddy"
DOCKER_HUB_IMAGE="ehyland/caddy"
CADDY_VERSION="0.10.12"

docker build -t "${IMAGE}:${CADDY_VERSION}" --build-arg "CADDY_VERSION=${CADDY_VERSION}" .
docker tag "${IMAGE}:${CADDY_VERSION}" "${IMAGE}:latest"

if [ "$1" == "--push" ]; then
    docker tag "${IMAGE}:${CADDY_VERSION}" "${REGISTRY}${IMAGE}:${CADDY_VERSION}"
    docker push "${REGISTRY}${IMAGE}:${CADDY_VERSION}"
    
    docker tag "${IMAGE}:${CADDY_VERSION}" "${REGISTRY}${IMAGE}:latest"
    docker push "${REGISTRY}${IMAGE}:latest"

    # docker tag "${IMAGE}:${CADDY_VERSION}" "${DOCKER_HUB_IMAGE}:latest"
    # docker push "${DOCKER_HUB_IMAGE}:latest"

    # docker tag "${IMAGE}:${CADDY_VERSION}" "${DOCKER_HUB_IMAGE}:${CADDY_VERSION}"
    # docker push "${DOCKER_HUB_IMAGE}:${CADDY_VERSION}"
fi