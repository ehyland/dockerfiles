#!/bin/sh

set -e

cd "$(dirname $0)"

NGINX_VERSION="1.13.2"
HHVM_VERSION="3.18"

IMAGE="registry.gitlab.com/ehyland/dockerfiles/nginx-hhvm"
VERSION="nginx_${NGINX_VERSION}-hhvm_${HHVM_VERSION}"

docker build \
    -t "${IMAGE}:${VERSION}" \
    --build-arg "NGINX_VERSION=${NGINX_VERSION}" \
    --build-arg "HHVM_VERSION=${HHVM_VERSION}" \
    .

if [ "$1" == "--push" ]; then
    docker push "${IMAGE}:${VERSION}"
fi

